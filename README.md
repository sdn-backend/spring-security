## 🍃스프링시큐리티 + 테스트 코드
> [참고 자료](https://github.com/kker5/spring-security-practice)
- `(usernamePassword)AuthenticationFilter`: 설정된 로그인 uri로 오는 요청을 감시하며, 유저 인증 처리
- `AuthenticationProvider`: DB정보와 화면에서 입력한 로그인정보 비교
- `UserDetailsService`: DB에서 유저 정보를 가져오는 역할
- `UserDetails` : 사용자의 정보를 담는 인터페이스 >> 그래서 보통 `class User implements UserDetails` ✔ 를 통해 사용!!


## 📢 테스트 구현 기술
### 1. `BDDAssertions`
- `then()` 검증

### 2. `MockMvc`
- 컨트롤러 테스트
- `perform`
    - 요청 전송 > 결과(ResultActions) 반환
- `andExpect`
    - 응답 검증
        - status(): 상태 검증
        - view(): 응답 받은 뷰 검증
        - redirect(): 응답받은 redirect 검증
        - content(): 응답 바디 검증
- `andDo`
    - 일반적으로 해야할 일 검증
    - `andDo(print()) 결과 print
### 3. `🍃스프링시큐리티 테스트`
- 종속성 추가 `testImplementation 'org.springframework.security:spring-security-test'`
- 기존 테스트와 다름
    - 로그인 한 상태로 서비스 이용했다는 가정!! 그래서, 마치 로그인한 것처럼 설정 가능
- 크게 3가지

#### 3-1. `@WithMockUser`
- 특정 사용자가 존재하는 것처럼 테스트 진행 가능
#### 3-2. `@WithUserDetails`
- 구현한 `UserDetailsService` 를 참고해 사용자를 가짜로 로그인 가능 
#### 3-3. `~.with`
- 직접 사용자를 mockMvc에 지정하는 방식
---
---
## 🤣테스트 코드

### `AdminControllerTest`
```java
@SpringBootTest
@ActiveProfiles(profiles = "test")
@Transactional
class AdminControllerTest {

    @Autowired
    private UserRepository userRepository;
    private MockMvc mockMvc;
    private User user;
    private User admin;

    @BeforeEach
    public void setUp(@Autowired WebApplicationContext applicationContext) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext)
                .apply(springSecurity()) // spring security 적용 ⭐
                .alwaysDo(print())
                .build();
        // ROLE_USER 권한이 있는 유저 생성
        user = userRepository.save(new User("user", "user", "ROLE_USER"));
        // ROLE_ADMIN 권한이 있는 관리자 생성
        admin = userRepository.save(new User("admin", "admin", "ROLE_ADMIN"));
    }

    @Test
    void getNoteForAdmin_인증없음() throws Exception {
        mockMvc.perform(get("/admin").with(csrf())) // csrf 토큰 추가
                .andExpect(redirectedUrlPattern("**/login"))
                .andExpect(status().is3xxRedirection()); // login이 안되있으므로 로그인 페이지로 redirect
    }

    @Test
    void getNoteForAdmin_어드민인증있음() throws Exception {
        mockMvc.perform(get("/admin").with(csrf()).with(user(admin))) // 어드민 추가
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void getNoteForAdmin_유저인증있음() throws Exception {
        mockMvc.perform(get("/admin").with(csrf()).with(user(user))) // 유저 추가
                .andExpect(status().isForbidden()); // 접근 거부
    }
}
```

### `NoteControllerTest`
```java
@SpringBootTest
@ActiveProfiles(profiles = "test")
@Transactional
class NoteControllerTest {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private NoteRepository noteRepository;
    private MockMvc mockMvc;
    private User user;
    private User admin;

    @BeforeEach
    public void setUp(@Autowired WebApplicationContext applicationContext) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext)
                .apply(springSecurity()) ⭐ 시큐리티 설정
                .alwaysDo(print())
                .build();
        user = userRepository.save(new User("user123", "user", "ROLE_USER"));
        admin = userRepository.save(new User("admin123", "admin", "ROLE_ADMIN"));
    }

    @Test
    void getNote_인증없음() throws Exception {
        mockMvc.perform(get("/note"))
                .andExpect(redirectedUrlPattern("**/login"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    // WithUserDetails 로 테스트 하는 방법 ⭐
    @WithUserDetails(
            value = "user123", // userDetailsService를 통해 가져올 수 있는 유저
            userDetailsServiceBeanName = "userDetailsService", // UserDetailsService 구현체의 Bean
            setupBefore = TestExecutionEvent.TEST_EXECUTION // 테스트 실행 직전에 유저를 가져온다.
    )
    void getNote_인증있음() throws Exception {
        mockMvc.perform(
                        get("/note")
                ).andExpect(status().isOk())
                .andExpect(view().name("note/index"))
                .andDo(print());
    }

    @Test
    void postNote_인증없음() throws Exception {
        mockMvc.perform(
                        post("/note").with(csrf())
                                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                                .param("title", "제목")
                                .param("content", "내용")
                ).andExpect(redirectedUrlPattern("**/login"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithUserDetails(
            value = "admin123",
            userDetailsServiceBeanName = "userDetailsService",
            setupBefore = TestExecutionEvent.TEST_EXECUTION
    )
    void postNote_어드민인증있음() throws Exception {
        mockMvc.perform(
                post("/note").with(csrf())
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("title", "제목")
                        .param("content", "내용")
        ).andExpect(status().isForbidden()); // 접근 거부
    }

    @Test
    @WithUserDetails(
            value = "user123",
            userDetailsServiceBeanName = "userDetailsService",
            setupBefore = TestExecutionEvent.TEST_EXECUTION
    )
    void postNote_유저인증있음() throws Exception {
        mockMvc.perform(
                post("/note").with(csrf())
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("title", "제목")
                        .param("content", "내용")
        ).andExpect(redirectedUrl("note")).andExpect(status().is3xxRedirection());
    }

    @Test
    void deleteNote_인증없음() throws Exception {
        Note note = noteRepository.save(new Note("제목", "내용", user));
        mockMvc.perform(
                        delete("/note?id=" + note.getId()).with(csrf())
                ).andExpect(redirectedUrlPattern("**/login"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithUserDetails(
            value = "user123",
            userDetailsServiceBeanName = "userDetailsService",
            setupBefore = TestExecutionEvent.TEST_EXECUTION
    )
    void deleteNote_유저인증있음() throws Exception {
        Note note = noteRepository.save(new Note("제목", "내용", user));
        mockMvc.perform(
                delete("/note?id=" + note.getId()).with(csrf())
        ).andExpect(redirectedUrl("note")).andExpect(status().is3xxRedirection());
    }

    @Test
    @WithUserDetails(
            value = "admin123",
            userDetailsServiceBeanName = "userDetailsService",
            setupBefore = TestExecutionEvent.TEST_EXECUTION
    )
    void deleteNote_어드민인증있음() throws Exception {
        Note note = noteRepository.save(new Note("제목", "내용", user));
        mockMvc.perform(
                delete("/note?id=" + note.getId()).with(csrf()).with(user(admin))
        ).andExpect(status().isForbidden()); // 접근 거부
    }
}
```

### `NoteServiceTest`
```java
@SpringBootTest
@ActiveProfiles(profiles = "test")
@Transactional
class NoteServiceTest {

    @Autowired
    private NoteService noteService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private NoteRepository noteRepository;

    @Test
    void findByUser_유저가_게시물조회() {
        // given
        User user = userRepository.save(new User("username", "password", "ROLE_USER"));
        noteRepository.save(new Note("title1", "content1", user));
        noteRepository.save(new Note("title2", "content2", user));
        // when
        List<Note> notes = noteService.findByUser(user);
        // then
        then(notes.size()).isEqualTo(2);
        Note note1 = notes.get(0);
        Note note2 = notes.get(1);

        // note1 = title2
        then(note1.getUser().getUsername()).isEqualTo("username");
        then(note1.getTitle()).isEqualTo("title2"); // 가장 늦게 insert된 데이터가 먼저 나와야합니다.
        then(note1.getContent()).isEqualTo("content2");
        // note2 = title1
        then(note2.getUser().getUsername()).isEqualTo("username");
        then(note2.getTitle()).isEqualTo("title1");
        then(note2.getContent()).isEqualTo("content1");
    }

    @Test
    void findByUser_어드민이_조회() {
        // given
        User admin = userRepository.save(new User("admin", "password", "ROLE_ADMIN"));
        User user1 = userRepository.save(new User("username", "password", "ROLE_USER"));
        User user2 = userRepository.save(new User("username2", "password", "ROLE_USER"));
        noteRepository.save(new Note("title1", "content1", user1));
        noteRepository.save(new Note("title2", "content2", user1));
        noteRepository.save(new Note("title3", "content3", user2));
        // when
        List<Note> notes = noteService.findByUser(admin);
        // then
        then(notes.size()).isEqualTo(3);
        Note note1 = notes.get(0);
        Note note2 = notes.get(1);
        Note note3 = notes.get(2);

        // note1 = title3
        then(note1.getUser().getUsername()).isEqualTo("username2");
        then(note1.getTitle()).isEqualTo("title3"); // 가장 늦게 insert된 데이터가 먼저 나와야합니다.
        then(note1.getContent()).isEqualTo("content3");
        // note2 = title2
        then(note2.getUser().getUsername()).isEqualTo("username");
        then(note2.getTitle()).isEqualTo("title2");
        then(note2.getContent()).isEqualTo("content2");
        // note3 = title1
        then(note3.getUser().getUsername()).isEqualTo("username");
        then(note3.getTitle()).isEqualTo("title1");
        then(note3.getContent()).isEqualTo("content1");
    }

    @Test
    void saveNote() {
        // given
        User user = userRepository.save(new User("username", "password", "ROLE_USER"));
        // when
        noteService.saveNote(user, "title1", "content1");
        // then
        then(noteRepository.count()).isOne();
    }

    @Test
    void deleteNote() {
        User user = userRepository.save(new User("username", "password", "ROLE_USER"));
        Note note = noteRepository.save(new Note("title1", "content1", user));
        noteService.deleteNote(user, note.getId());
        // then
        then(noteRepository.count()).isZero();
    }
}
```

### `NoticeControllerTest`
```java
@SpringBootTest
@Transactional
class NoticeControllerTest {

    @Autowired
    private NoticeRepository noticeRepository;
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp(@Autowired WebApplicationContext applicationContext) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext)
                .apply(springSecurity()) ⭐ 스프링 시큐리티 설정
                .alwaysDo(print())
                .build();
    }

    @Test
    void getNotice_인증없음() throws Exception {
        mockMvc.perform(get("/notice"))
                .andExpect(redirectedUrlPattern("**/login"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser
    void getNotice_인증있음() throws Exception {
        mockMvc.perform(get("/notice"))
                .andExpect(status().isOk())
                .andExpect(view().name("notice/index"));
    }

    @Test
    void postNotice_인증없음() throws Exception {
        mockMvc.perform(
                post("/notice")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("title", "제목")
                        .param("content", "내용")
        ).andExpect(status().isForbidden()); // 접근 거부
    }

    @Test
    @WithMockUser(roles = {"USER"}, username = "admin", password = "admin")
    void postNotice_유저인증있음() throws Exception {
        mockMvc.perform(
                post("/notice").with(csrf())
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("title", "제목")
                        .param("content", "내용")
        ).andExpect(status().isForbidden()); // 접근 거부
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "admin", password = "admin")
    void postNotice_어드민인증있음() throws Exception {
        mockMvc.perform(
                post("/notice").with(csrf())
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("title", "제목")
                        .param("content", "내용")
        ).andExpect(redirectedUrl("notice")).andExpect(status().is3xxRedirection());
    }

    @Test
    void deleteNotice_인증없음() throws Exception {
        Notice notice = noticeRepository.save(new Notice("제목", "내용"));
        mockMvc.perform(
                delete("/notice?id=" + notice.getId())
        ).andExpect(status().isForbidden()); // 접근 거부
    }

    @Test
    @WithMockUser(roles = {"USER"}, username = "admin", password = "admin")
    void deleteNotice_유저인증있음() throws Exception {
        Notice notice = noticeRepository.save(new Notice("제목", "내용"));
        mockMvc.perform(
                delete("/notice?id=" + notice.getId()).with(csrf())
        ).andExpect(status().isForbidden()); // 접근 거부
    }

    @Test
    @WithMockUser(roles = {"ADMIN"}, username = "admin", password = "admin")
    void deleteNotice_어드민인증있음() throws Exception {
        Notice notice = noticeRepository.save(new Notice("제목", "내용"));
        mockMvc.perform(
                delete("/notice?id=" + notice.getId()).with(csrf())
        ).andExpect(redirectedUrl("notice")).andExpect(status().is3xxRedirection());
    }
}
```

### `SignUpControllerTest`
```java
@SpringBootTest
@Transactional
class SignUpControllerTest {

    private MockMvc mockMvc;

    @BeforeEach
    public void setUp(@Autowired WebApplicationContext applicationContext) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext)
                .apply(springSecurity())
                .alwaysDo(print())
                .build();
    }

    @Test
    void signup() throws Exception {
        mockMvc.perform(
                post("/signup").with(csrf())
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("username", "user123")
                        .param("password", "password")
        ).andExpect(redirectedUrl("login")).andExpect(status().is3xxRedirection());
    }
}
```

### `UserServiceTest`
```java
@SpringBootTest
@ActiveProfiles(profiles = "test")
@Transactional
class UserServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;

    @Test
    void signup() {
        //given
        String username = "user123";
        String password = "password";
        //when
        User user = userService.signup(username, password);
        //then
        then(user.getId()).isNotNull(); // id가 NotNull인지 검증
        then(user.getUsername()).isEqualTo("user123"); // 유저명이 user123인지 검증
        then(user.getPassword()).startsWith("{bcrypt}"); // 패스워드가 {bcrypt}로 시작하는지 검증
        then(user.getAuthorities()).hasSize(1); // Authorities가 1개인지 검증
        then(user.getAuthorities().stream().findFirst().get().getAuthority()).isEqualTo("ROLE_USER");
        then(user.isAdmin()).isFalse(); // 어드민 여부가 False인지 검증
        then(user.isAccountNonExpired()).isTrue();
        then(user.isAccountNonLocked()).isTrue();
        then(user.isEnabled()).isTrue();
        then(user.isCredentialsNonExpired()).isTrue();
    }

    @Test
    void signupAdmin() {
        //given
        String username = "admin123";
        String password = "password";
        //when
        User user = userService.signupAdmin(username, password);
        //then
        then(user.getId()).isNotNull();
        then(user.getUsername()).isEqualTo("admin123");
        then(user.getPassword()).startsWith("{bcrypt}");
        then(user.getAuthorities()).hasSize(1);
        then(user.getAuthorities().stream().findFirst().get().getAuthority()).isEqualTo("ROLE_ADMIN");
        then(user.isAdmin()).isTrue();
        then(user.isAccountNonExpired()).isTrue();
        then(user.isAccountNonLocked()).isTrue();
        then(user.isEnabled()).isTrue();
        then(user.isCredentialsNonExpired()).isTrue();
    }

    @Test
    void findByUsername() {
        //given
        userRepository.save(new User("user123", "password", "ROLE_USER"));
        //when
        User user = userService.findByUsername("user123");
        //then
        then(user.getId()).isNotNull();
    }
}
```
